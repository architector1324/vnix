#/bin/bash

# build vnix kernel
echo 'Building vnix kernel.'
cd ..
./build.sh
cd ./linux

# build rootfs
echo 'Building rootfs.'
mkdir rootfs
cd ./rootfs
mkdir -p ./fonts
cp ../../target/x86_64-unknown-linux-musl/release/vnix ./init
cp ../fonts/* ./fonts

mkdir dev proc sys
chmod +x init

find . | cpio -R root:root -H newc -o | gzip > ../iso/rootfs.gz
cd ..

echo 'Building iso.'
cd ./iso
xorriso \
    -as mkisofs \
    -o ../vnix-linux.iso \
    -b isolinux.bin \
    -c boot.cat \
    -no-emul-boot \
    -boot-load-size 4 \
    -boot-info-table \
    ./
cd ..
