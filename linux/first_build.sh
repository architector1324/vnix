#/bin/bash

LINUX_VERSION_PREFIX=5
LINUX_VERSION=$LINUX_VERSION_PREFIX.3.4
SYSLINUX_VERSION=6.03
THREADS=$(nproc --all)

linux(){
    echo 'Fetching sources.'
    wget http://kernel.org/pub/linux/kernel/v$LINUX_VERSION_PREFIX.x/linux-$LINUX_VERSION.tar.xz

    echo 'Unpacking sources.'
    tar -xvf linux-$LINUX_VERSION.tar.xz

    # compiling linux
    echo 'Compiling linux.'
    cd ./linux-$LINUX_VERSION
    mkdir build
    make -j$THREADS O=./build distclean defconfig
    cd ./build
    sed -i "s|.*CONFIG_FB_VESA.*|CONFIG_FB_VESA=y|" .config
    sed -i "s|.*CONFIG_FB_EFI.*|CONFIG_FB_EFI=y|" .config
    sed -i "s|.*CONFIG_FB_NVIDIA.*|CONFIG_FB_NVIDIA=m|" .config
    sed -i "s|CONFIG_LOGO=y|CONFIG_LOGO=n|" .config
    make menuconfig
    make -j$THREADS
    make -j$THREADS modules
    INSTALL_MOD_PATH=../../rootfs make modules_install
    cp ./arch/x86/boot/bzImage ../../iso/kernel.gz
    cd ../../

    # cleanup
    echo 'Cleanup.'
    rm -rf linux-*
}

syslinux(){
    echo 'Fetching sources.'
    wget http://kernel.org/pub/linux/utils/boot/syslinux/syslinux-$SYSLINUX_VERSION.tar.xz

    echo 'Unpacking sources.'
    tar -xvf syslinux-$SYSLINUX_VERSION.tar.xz

    # setup syslinux
    echo 'Setup syslinux.'
    cd ./iso
    cp ../syslinux-$SYSLINUX_VERSION/bios/core/isolinux.bin .
    cp ../syslinux-$SYSLINUX_VERSION/bios/com32/elflink/ldlinux/ldlinux.c32 .
    cp ../syslinux-$SYSLINUX_VERSION/efi64/efi/syslinux.efi .
    cp ../syslinux-$SYSLINUX_VERSION/efi64/com32/elflink/ldlinux/ldlinux.e64 .
    echo 'default kernel.gz initrd=rootfs.gz vga=0x0343' > ./isolinux.cfg
    cd ..

    # cleanup
    echo 'Cleanup.'
    rm -rf syslinux-*
}

mkdir iso
linux
syslinux

./build.sh



