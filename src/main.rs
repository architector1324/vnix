mod vnix;
mod kernel;
mod tests;

fn main() {
    vnix::init();

    match kernel::vanilla() {
        Ok(_) => vnix::poweroff(5),
        Err(e) => {
            println!("CRIT [kern]: kernel panic: {}", e);
            vnix::poweroff(10);
        }
    }
}
