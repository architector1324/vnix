use super::vnix;
use vnix::core::node::Node;
use vnix::proc::gen;


pub fn vanilla() -> Result<(), String> {
    println!("INFO [kern]: vnix `vanilla` kernel started.");

    // init nodes
    let mut gen_int: Node<gen::GenInt> = Node::new("gen.int");
    let mut gen_float: Node<gen::GenFloat> = Node::new("gen.float");

    println!("INFO [kern]: node `gen.int` ready.");
    println!("INFO [kern]: node `gen.float` ready.");

    // try to comment this line and see what will happen
    gen_int.slot(0).gen()?;
    gen_float.slot(0).gen()?;

    println!("INFO [gen.int]: generated msg {}.", gen_int.slot(0).out_port(0).recv()?);
    println!("INFO [gen.float]: generated msg {}.", gen_float.slot(0).out_port(0).recv()?);

    Ok(())
}
