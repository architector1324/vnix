use super::super::super::vnix::utils::parser;
use super::super::super::vnix::utils::parser::{Token, Parser};

#[test]
fn parse_primitives() {
    let mut src = r#"Mya1bcd234"hello⟁'world'!"'helloⵘ"world"!"#.chars();

    // parse empty
    let mut p = parser::Empty;
    assert_eq!(
        Token::empty("EMPTY"),
        p.next(&mut src).unwrap()
    );

    // parse name
    let mut p = parser::Name{
        name: "My".into()
    };

    assert_eq!(
        Token::from("STRING", "My"),
        p.next(&mut src).unwrap()
    );

    let mut src2 = "M123y".chars();
    assert_eq!(Err("can not parse name `My`!".to_string()), p.next(&mut src2));
    assert_eq!("M123y", src2.as_str());

    // parse letter
    let mut p = parser::Letter;
    assert_eq!(
        Token::from("STRING", "a"),
        p.next(&mut src).unwrap()
    );

    let mut src2 = "123".chars();
    assert_eq!(Err("can not parse letter!".to_string()), p.next(&mut src2));
    assert_eq!("123", src2.as_str());

    // parse digit
    let mut p = parser::Digit;
    assert_eq!(
        Token::from("INT", "1"),
        p.next(&mut src).unwrap()
    );

    let mut src2 = "abc".chars();
    assert_eq!(Err("can not parse digit!".to_string()), p.next(&mut src2));
    assert_eq!("abc", src2.as_str());

    // parse word
    let mut p = parser::Word;
    assert_eq!(
        Token::from("STRING", "bcd"),
        p.next(&mut src).unwrap()
    );

    let mut src2 = "abc123".chars();
    assert_eq!(
        Token::from("STRING", "abc"),
        p.next(&mut src2).unwrap()
    );
    assert_eq!("123", src2.as_str());

    // parse int
    let mut p = parser::Int;
    assert_eq!(
        Token::from("INT", "234"),
        p.next(&mut src).unwrap()
    );

    let mut src2 = "123abc".chars();
    assert_eq!(
        Token::from("INT", "123"),
        p.next(&mut src2).unwrap()
    );
    assert_eq!("abc", src2.as_str());
}

#[test]
fn parse_complex() {
    let mut src = "abc123abc 123My123abc!def ABC ABC   ".chars();

    // parse ident
    let mut p = parser::Ident{
        ident: "VAR".into(),
        it: parser::Word
    };

    assert_eq!(
        Token::from("VAR", "abc"),
        p.next(&mut src).unwrap()
    );

    // parse option
    let mut p = parser::Optional{
        it: parser::Word
    };

    assert_eq!(
        Token::empty("EMPTY"),
        p.next(&mut src).unwrap()
    );

    let mut p = parser::Optional{
        it: parser::Int
    };

    assert_eq!(
        Token::from("INT", "123"),
        p.next(&mut src).unwrap()
    );

    // parse concat
    let mut p = parser::Concat{
        vars: vec![
            Box::new(parser::Word),
            Box::new(parser::space()),
            Box::new(parser::Int)
        ]
    };

    assert_eq!(
        Token::tree("LIST", vec![
            Token::from("STRING", "abc"),
            Token::empty("EMPTY"),
            Token::from("INT", "123"),
        ]),
        p.next(&mut src).unwrap()
    );

    // parse alter
    let mut p = parser::Alter{
        vars: vec![
            Box::new(parser::Name{name: "My".into()}),
            Box::new(parser::Word),
            Box::new(parser::Int)
        ]
    };

    assert_eq!(
        Token::from("STRING", "My"),
        p.next(&mut src).unwrap()
    );

    assert_eq!(
        Token::from("INT", "123"),
        p.next(&mut src).unwrap()
    );

    assert_eq!(
        Token::from("STRING", "abc"),
        p.next(&mut src).unwrap()
    );

    // parse exclude
    let mut p = parser::Exclude {
        it: parser::Name{
            name: "!".into()
        }
    };

    assert_eq!(
        Token::empty("EMPTY"),
        p.next(&mut src).unwrap()
    );

    // parse group
    let mut p = parser::Group {
        it: parser::Word
    };
    assert_eq!(
        Token::tree("GROUP", vec![Token::from("STRING", "def")]),
        p.next(&mut src).unwrap()
    );

    // parse space
    let mut p = parser::space();
    assert_eq!(
        Token::empty("EMPTY"),
        p.next(&mut src).unwrap()
    );

    // parse repeat
    let mut p = parser::Repeat{
        it: parser::Concat{
            vars: vec![
                Box::new(parser::Word),
                Box::new(parser::space())
            ]
        }
    };

    assert_eq!(
        Token::tree("LIST", vec![
            Token::tree("LIST", vec![
                Token::from("STRING", "ABC"),
                Token::empty("EMPTY")
            ]),
            Token::tree("LIST", vec![
                Token::from("STRING", "ABC"),
                Token::empty("EMPTY")
            ])
        ]),
        p.next(&mut src).unwrap()
    );

    assert_eq!(
        Token::empty("EMPTY"),
        p.next(&mut src).unwrap()
    );
    
    // spaces
    let mut p = parser::spaces();
    assert_eq!(
        Token::empty("EMPTY"),
        p.next(&mut src).unwrap()
    );
}

#[test]
fn list_handlers() {
    let mut src = "abc ABC  def abc".chars();

    // extend sublists
    let mut p = parser::ListExtend {
        it: parser::Repeat {
            it: parser::Concat {
                vars: vec![
                    Box::new(parser::Word),
                    Box::new(parser::space())
                ]
            }
        }
    };

    assert_eq!(
        Token::tree("LIST", vec![
            Token::from("STRING", "abc"),
            Token::empty("EMPTY"),
            Token::from("STRING", "ABC"),
            Token::empty("EMPTY")
        ]),
        p.next(&mut src).unwrap()
    );

    // remove empty
    let mut p = parser::ListRmEmpty {
        it: parser::Concat {
            vars: vec![
                Box::new(parser::space()),
                Box::new(parser::Word)
            ]
        }
    };

    assert_eq!(
        Token::tree("LIST", vec![
            Token::from("STRING", "def"),
        ]),
        p.next(&mut src).unwrap()
    );

    // expand list
    let mut p = parser::ListExpand {
        it: parser::ListRmEmpty {
            it: parser::Concat {
                vars: vec![
                    Box::new(parser::space()),
                    Box::new(parser::Word)
                ]
            }
        }
    };

    assert_eq!(
        Token::from("STRING", "abc"),
        p.next(&mut src).unwrap()
    );
}

#[test]
fn numbers_parser() {
    let mut src = "123 -3  3.14   -2.71".chars();

    /*
        int = [-] INT
        float = [-] INT `.` INT
        num = (float | int)
        parser = {num SPACES}
    */

    let p_int = parser::Ident {
        ident: "int".into(),
        it: parser::ListRmEmpty {
            it: parser::Concat{
                vars: vec![
                    Box::new(parser::Optional {
                        it: parser::Name{
                            name: "-".into()
                        }
                    }),
                    Box::new(parser::Int)
                ]
            }
        }
    };

    let p_float = parser::Ident {
        ident: "float".into(),
        it: parser::ListRmEmpty {
            it: parser::Concat{
                vars: vec![
                    Box::new(parser::Optional {
                        it: parser::Name{
                            name: "-".into()
                        }
                    }),
                    Box::new(parser::Int),
                    Box::new(parser::Name{
                        name: ".".into()
                    }),
                    Box::new(parser::Int)
                ]
            }
        }
    };

    let p_num = parser::Alter {
        vars: vec![
            Box::new(p_float),
            Box::new(p_int)
        ]
    };

    let mut p = parser::Repeat{
        it: parser::ListExpand {
            it: parser::ListRmEmpty {
                it: parser::Concat {
                    vars: vec![
                        Box::new(p_num),
                        Box::new(parser::spaces())
                    ]
                }
            }
        }
    };

    assert_eq!(
        Token::tree("LIST", vec![
            Token::tree("int", vec![
                Token::from("INT", "123")
            ]),
            Token::tree("int", vec![
                Token::from("STRING", "-"),
                Token::from("INT", "3")
            ]),
            Token::tree("float", vec![
                Token::from("INT", "3"),
                Token::from("STRING", "."),
                Token::from("INT", "14")
            ]),
            Token::tree("float", vec![
                Token::from("STRING", "-"),
                Token::from("INT", "2"),
                Token::from("STRING", "."),
                Token::from("INT", "71")
            ])
        ]),
        p.next(&mut src).unwrap()
    );
}