use super::super::super::vnix::utils::pack::{Encode, Marker};

#[test]
fn basic_encoding() {
    // test bool encoding
    let x = true;
    let buf = x.encode().unwrap();
    assert_eq!([Marker::Bool.to_u8(), 0x1], buf[..]);

    let (y, _) = bool::decode(&buf).unwrap();
    assert_eq!(x, y);

    let x = false;
    let buf = x.encode().unwrap();
    assert_eq!([Marker::Bool.to_u8(), 0], buf[..]);

    let (y, _) = bool::decode(&buf).unwrap();
    assert_eq!(x, y);
    
    // test u8 and i8 encoding
    let x = 0x4f;
    let buf = x.encode().unwrap();
    assert_eq!([Marker::U8.to_u8(), 0x4f], buf[..]);

    let (y, _) = u8::decode(&buf).unwrap();
    assert_eq!(x, y);

    let x = 0x4f;
    let buf = x.encode().unwrap();
    assert_eq!([Marker::I8.to_u8(), 0x4f], buf[..]);

    let (y, _) = i8::decode(&buf).unwrap();
    assert_eq!(x, y);

    // test u16 and i16 encoding
    let x = 0x4f6c;
    let buf = x.encode().unwrap();
    assert_eq!([Marker::U16.to_u8(), 0x6c, 0x4f], buf[..]);

    let (y, _) = u16::decode(&buf).unwrap();
    assert_eq!(x, y);

    let x = 0x4f6c;
    let buf = x.encode().unwrap();
    assert_eq!([Marker::I16.to_u8(), 0x6c, 0x4f], buf[..]);

    let (y, _) = i16::decode(&buf).unwrap();
    assert_eq!(x, y);

    // test u32 and i32 encoding
    let x = 0x4f6cff7d;
    let buf = x.encode().unwrap();
    assert_eq!([Marker::U32.to_u8(), 0x7d, 0xff, 0x6c, 0x4f], buf[..]);

    let (y, _) = u32::decode(&buf).unwrap();
    assert_eq!(x, y);

    let x = 0x4f6cff7d;
    let buf = x.encode().unwrap();
    assert_eq!([Marker::I32.to_u8(), 0x7d, 0xff, 0x6c, 0x4f], buf[..]);

    let (y, _) = i32::decode(&buf).unwrap();
    assert_eq!(x, y);

    // test u64 and i64 encoding
    let x = 0x1c3dff5a4f6cff7d;
    let buf = x.encode().unwrap();
    assert_eq!([Marker::U64.to_u8(), 0x7d, 0xff, 0x6c, 0x4f, 0x5a, 0xff, 0x3d, 0x1c], buf[..]);

    let (y, _) = u64::decode(&buf).unwrap();
    assert_eq!(x, y);

    let x = 0x1c3dff5a4f6cff7d;
    let buf = x.encode().unwrap();
    assert_eq!([Marker::I64.to_u8(), 0x7d, 0xff, 0x6c, 0x4f, 0x5a, 0xff, 0x3d, 0x1c], buf[..]);

    let (y, _) = i64::decode(&buf).unwrap();
    assert_eq!(x, y);

    // test f32 and f64 encoding
    let x = 3.14;
    let buf = x.encode().unwrap();
    assert_eq!([Marker::F32.to_u8(), 0xc3, 0xf5, 0x48, 0x40], buf[..]);

    let (y, _) = f32::decode(&buf).unwrap();
    assert_eq!(x, y);

    let x = 2.71;
    let buf = x.encode().unwrap();
    assert_eq!([Marker::F64.to_u8(), 0xae, 0x47, 0xe1, 0x7a, 0x14, 0xae, 0x5, 0x40], buf[..]);

    let (y, _) = f64::decode(&buf).unwrap();
    assert_eq!(x, y);
}

#[test]
fn compound_ecoding() {
    // test str encoding
    let x = String::from("λ☢abc☣");
    let buf = x.encode().unwrap();
    assert_eq!([
        Marker::Str.to_u8(),
        0xb, 0, 0, 0, 0, 0, 0, 0,
        0xce, 0xbb, 0xe2, 0x98, 0xa2, 0x61, 0x62, 0x63, 0xe2, 0x98, 0xa3
    ], buf[..]);

    let (y, _) = String::decode(&buf).unwrap();
    assert_eq!(x, y);

    // test bin encoding
    let x: Vec<u8> = vec![0x1f, 0x4c, 0xff];
    let buf = x.encode().unwrap();
    assert_eq!([Marker::Bin.to_u8(), 0x3, 0, 0, 0, 0, 0, 0, 0, 0x1f, 0x4c, 0xff], buf[..]);

    let (y, _) = Vec::<u8>::decode(&buf).unwrap();
    assert_eq!(x, y);

    // test pair
    let x = (123 as i64, 3.14 as f32);
    let buf = x.encode().unwrap();
    assert_eq!([
        Marker::Pair.to_u8(),
        Marker::I64.to_u8(), 0x7b, 0, 0, 0, 0, 0, 0, 0,
        Marker::F32.to_u8(), 0xc3, 0xf5, 0x48, 0x40
    ], buf[..]);

    let (y, _) = <(i64, f32)>::decode(&buf).unwrap();
    assert_eq!(x, y);
}