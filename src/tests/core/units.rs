use super::super::super::vnix::utils::pack::{Encode, Marker};
use super::super::super::vnix::core::units::{Unit, UnitMarker, Int, Float};
use num::bigint::ToBigInt;
use num::rational::Ratio;

#[test]
fn encoding() {
    // test bool encoding
    let x = Unit::Bool(true);
    let buf = x.encode().unwrap();
    assert_eq!([Marker::Bool.to_u8(), 0x1], buf[..]);

    let (y, _) = Unit::decode(&buf).unwrap();
    assert_eq!(x, y);

    let x = Unit::Bool(false);
    let buf = x.encode().unwrap();
    assert_eq!([Marker::Bool.to_u8(), 0], buf[..]);

    let (y, _) = Unit::decode(&buf).unwrap();
    assert_eq!(x, y);

    // test byte encoding
    let x = Unit::Byte(0x4f);
    let buf = x.encode().unwrap();
    assert_eq!([Marker::U8.to_u8(), 0x4f], buf[..]);

    let (y, _)  = Unit::decode(&buf).unwrap();
    assert_eq!(x, y);

    // test int encoding
    let x = Unit::Int(Int::Def(123));
    let buf = x.encode().unwrap();
    assert_eq!([Marker::I64.to_u8(), 0x7b, 0, 0, 0, 0, 0, 0, 0], buf[..]);

    let (y, _) = Unit::decode(&buf).unwrap();
    assert_eq!(x, y);

    let x = Unit::Int(Int::Big(-123.to_bigint().unwrap()));
    let buf = x.encode().unwrap();
    assert_eq!([
        UnitMarker::BigInt.to_u8(), Marker::Pair.to_u8(), 0x1,
        Marker::Bin.to_u8(), 0x1, 0, 0, 0, 0, 0, 0, 0, 0x7b
    ], buf[..]);

    let (y, _) = Unit::decode(&buf).unwrap();
    assert_eq!(x, y);

    // test float encoding
    let x = Unit::Float(Float::Def(3.14));
    let buf = x.encode().unwrap();
    assert_eq!([Marker::F64.to_u8(), 0x1f, 0x85, 0xeb, 0x51, 0xb8, 0x1e, 0x9, 0x40], buf[..]);

    let (y, _) = Unit::decode(&buf).unwrap();
    assert_eq!(x, y);

    let x = Unit::Float(Float::Big(Ratio::from_float(3.14).unwrap()));
    let buf = x.encode().unwrap();

    assert_eq!(&[
        UnitMarker::BigRational.to_u8(), Marker::Pair.to_u8(), 
        Marker::Pair.to_u8(), 0, Marker::Bin.to_u8(), 0x7, 0, 0, 0, 0, 0, 0, 0, 0x1f, 0x85, 0xeb, 0x51, 0xb8, 0x1e, 0x19,
        Marker::Pair.to_u8(), 0, Marker::Bin.to_u8(), 0x7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8
    ][..], &buf[..]);

    let (y, _) = Unit::decode(&buf).unwrap();
    assert_eq!(x, y);

    // test string encoding
    let x = Unit::Str(String::from("λ☢abc☣"));
    let buf = x.encode().unwrap();
    assert_eq!([
        Marker::Str.to_u8(),
        0xb, 0, 0, 0, 0, 0, 0, 0,
        0xce, 0xbb, 0xe2, 0x98, 0xa2, 0x61, 0x62, 0x63, 0xe2, 0x98, 0xa3
    ], buf[..]);

    let (y, _) = Unit::decode(&buf).unwrap();
    assert_eq!(x, y);

    // test list encoding
    let x = Unit::List(vec![Unit::Int(Int::Def(123)), Unit::Float(Float::Def(3.14))]);
    let buf = x.encode().unwrap();
    assert_eq!(&[
        UnitMarker::List.to_u8(), Marker::Pair.to_u8(), Marker::I64.to_u8(),
        0x2, 0, 0, 0, 0, 0, 0, 0,
        Marker::Bin.to_u8(), 0x12, 0, 0, 0, 0, 0, 0, 0,
        Marker::I64.to_u8(), 0x7b, 0, 0, 0, 0, 0, 0, 0,
        Marker::F64.to_u8(), 0x1f, 0x85, 0xeb, 0x51, 0xb8, 0x1e, 0x9, 0x40
    ][..], &buf[..]);

    let (y, _) = Unit::decode(&buf).unwrap();
    assert_eq!(x, y);
    
    // test map encoding
    let x = Unit::Map(vec![(Unit::Int(Int::Def(123)), Unit::Float(Float::Def(3.14)))]);
    let buf = x.encode().unwrap();
    assert_eq!(&[
        UnitMarker::Map.to_u8(), Marker::Pair.to_u8(), Marker::I64.to_u8(),
        0x1, 0, 0, 0, 0, 0, 0, 0,
        Marker::Bin.to_u8(), 0x13, 0, 0, 0, 0, 0, 0, 0,
        Marker::Pair.to_u8(),
        Marker::I64.to_u8(), 0x7b, 0, 0, 0, 0, 0, 0, 0,
        Marker::F64.to_u8(), 0x1f, 0x85, 0xeb, 0x51, 0xb8, 0x1e, 0x9, 0x40
    ][..], &buf[..]);

    let (y, _) = Unit::decode(&buf).unwrap();
    assert_eq!(x, y);

    // test msg encoding
    let x = Unit::Msg(vec![(String::from("float"), Unit::Float(Float::Def(3.14)))]);
    let buf = x.encode().unwrap();
    assert_eq!(&[
        UnitMarker::Msg.to_u8(), Marker::Pair.to_u8(), Marker::I64.to_u8(),
        0x1, 0, 0, 0, 0, 0, 0, 0,
        Marker::Bin.to_u8(), 0x18, 0, 0, 0, 0, 0, 0, 0,
        Marker::Pair.to_u8(),
        Marker::Str.to_u8(), 0x5, 0, 0, 0, 0, 0, 0, 0, 0x66, 0x6c, 0x6f, 0x61, 0x74,
        Marker::F64.to_u8(), 0x1f, 0x85, 0xeb, 0x51, 0xb8, 0x1e, 0x9, 0x40
    ][..], &buf[..]);

    let (y, _) = Unit::decode(&buf).unwrap();
    assert_eq!(x, y);
}
