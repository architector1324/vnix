use super::super::super::vnix::core::msg::Msg;
use super::super::super::vnix::core::types::{Type, Int, Float};

#[test]
fn display() {
    let msg = Msg::from(vec![
        (Type::Str("a".into()), Type::Int(Int::Def(123))),
        (Type::Str("b".into()), Type::Float(Float::Def(3.14))),
        (Type::Str("c".into()), Type::Bool(true)),
        (Type::Str("d".into()), Type::Byte(0x4f))
    ]);

    assert_eq!("{`a`:123 `b`:3.14 `c`:t `d`:4f}", msg.to_string());
}