use super::super::super::vnix::core::node::{Node, Proc};
use super::super::super::vnix::core::slot;
use super::super::super::vnix::core::port;
use super::super::super::vnix::core::msg;
// use std::thread;

struct TestNodeProc;

impl Proc for TestNodeProc {
    fn process(slot: &mut slot::Slot<Self>) -> Result<(), String>{
        slot.out_port(13).send(msg::Msg::new())?;
        Ok(())
    }
}

#[test]
fn port_send_and_recv_timeout(){
    let mut port = port::Port::new(13);
    let msg = msg::Msg::new();

    assert_eq!(Err(String::from("port13: can not recv msg: timed out!")), port.recv());
    port.send(msg.clone()).unwrap();
    assert_eq!(Err(String::from("port13: can not send msg {}: timed out!")), port.send(msg));
}

#[test]
fn new_node(){
    let mut node: Node<TestNodeProc> = Node::new("test.node");

    assert_eq!("test.node", node.name);
    assert_eq!(123, node.slot(123).id);
    assert_eq!(46,  node.slot(123).in_port(46).id);
}

#[test]
fn node_self_send_and_recv(){
    let mut node: Node<TestNodeProc> = Node::new("test.node");

    let msg = msg::Msg::new();
    node.slot(0).out_port(13).send(msg).unwrap();

    let msg = node.slot(0).out_port(13).recv().unwrap();
    node.slot(0).in_port(46).send(msg).unwrap();
}

#[test]
fn node_gen() {
    let mut node: Node<TestNodeProc> = Node::new("test.node");
    assert_eq!(Ok(()), node.slot(0).gen());
    assert_eq!("{}", node.slot(0).out_port(13).recv().unwrap().to_string());
}