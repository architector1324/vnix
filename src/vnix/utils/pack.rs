//-------------------------------------
//-           DEFINITIONS             -
//-------------------------------------

// any object that can be converted to bytes stream
pub trait Encode: Sized {
    fn encode(&self) -> Result<Vec<u8>, String>;
    fn decode(what: &[u8]) -> Result<(Self, usize), String>;
}

#[derive(Debug, Clone, Copy)]
pub enum Marker {
    Bool = 0,
    U8, U16, U32, U64,
    I8, I16, I32, I64,
    F32, F64,
    Str,
    Bin,
    Pair
}


//-------------------------------------
//-         IMPLEMENTATIONS           -
//-------------------------------------

impl Marker {
    pub fn to_u8(&self) -> u8 {
        *self as u8
    }

    pub fn from_u8(what: u8) -> Result<Marker, String> {
        match what {
            sig if sig == Marker::Bool.to_u8() => Ok(Marker::Bool),
            sig if sig == Marker::U8.to_u8() => Ok(Marker::U8),
            sig if sig == Marker::U16.to_u8() => Ok(Marker::U16),
            sig if sig == Marker::U32.to_u8() => Ok(Marker::U32),
            sig if sig == Marker::U64.to_u8() => Ok(Marker::U64),
            sig if sig == Marker::I8.to_u8() => Ok(Marker::I8),
            sig if sig == Marker::I16.to_u8() => Ok(Marker::I16),
            sig if sig == Marker::I32.to_u8() => Ok(Marker::I32),
            sig if sig == Marker::I64.to_u8() => Ok(Marker::I64),
            sig if sig == Marker::F32.to_u8() => Ok(Marker::F32),
            sig if sig == Marker::F64.to_u8() => Ok(Marker::F64),
            sig if sig == Marker::Str.to_u8() => Ok(Marker::Str),
            sig if sig == Marker::Bin.to_u8() => Ok(Marker::Bin),
            sig if sig == Marker::Pair.to_u8() => Ok(Marker::Pair),
            _ => Err(String::from("can not decode marker: unknow signature!"))
        }
    }
}

// basic types 
macro_rules! decode_fixed {
    ($what:expr, $marker:pat, $len:expr, $name:expr, $decode:expr) => {
        match $what.len() {
            0 => Err(format!("can not decode {}: buffer is empty!", $name)),
            l if l >= $len => 
            match Marker::from_u8($what[0])? {
                $marker => $decode,
                _ => Err(format!("can not decode {}: invalid signature!", $name))
            },
            _ => Err(format!("can not decode {}: invalid buffer size!", $name))
        }
    };
}

impl Encode for bool {
    fn encode(&self) -> Result<Vec<u8>, String> {
        Ok(vec![
            Marker::Bool.to_u8(),
            match self {
                true => 0x1,
                false => 0
            }
        ])
    }

    fn decode(what: &[u8]) -> Result<(bool, usize), String> {
        decode_fixed!(what, Marker::Bool, 2, "bool",
        match what[1] {
            0x1 => Ok((true, 2)),
            0 => Ok((false, 2)),
            _ => Err(String::from("can not decode bool: invalid value!"))
        })
    }
}

impl Encode for u8 {
    fn encode(&self) -> Result<Vec<u8>, String> {
        Ok(vec![Marker::U8.to_u8(), *self])
    }

    fn decode(what: &[u8]) -> Result<(u8, usize), String> {
        decode_fixed!(what, Marker::U8, 2, "u8", Ok((what[1], 2)))
    }
}

impl Encode for u16 {
    fn encode(&self) -> Result<Vec<u8>, String> {
        let mut buf = vec![Marker::U16.to_u8()];
        buf.extend_from_slice(&self.to_le_bytes());
        Ok(buf)
    }

    fn decode(what: &[u8]) -> Result<(u16, usize), String> {
        decode_fixed!(what, Marker::U16, 3, "u16", 
            Ok((u16::from_le_bytes(unsafe{*(what[1..3].as_ptr() as *const [u8; 2])}), 3))
        )
    }
}

impl Encode for u32 {
    fn encode(&self) -> Result<Vec<u8>, String> {
        let mut buf = vec![Marker::U32.to_u8()];
        buf.extend_from_slice(&self.to_le_bytes());
        Ok(buf)
    }

    fn decode(what: &[u8]) -> Result<(u32, usize), String> {
        decode_fixed!(what, Marker::U32, 5, "u32", 
            Ok((u32::from_le_bytes(unsafe{*(what[1..5].as_ptr() as *const [u8; 4])}), 5))
        )
    }
}

impl Encode for u64 {
    fn encode(&self) -> Result<Vec<u8>, String> {
        let mut buf = vec![Marker::U64.to_u8()];
        buf.extend_from_slice(&self.to_le_bytes());
        Ok(buf)
    }

    fn decode(what: &[u8]) -> Result<(u64, usize), String> {
        decode_fixed!(what, Marker::U64, 9, "u64", 
            Ok((u64::from_le_bytes(unsafe{*(what[1..9].as_ptr() as *const [u8; 8])}), 9))
        )
    }
}

impl Encode for i8 {
    fn encode(&self) -> Result<Vec<u8>, String> {
        Ok(vec![Marker::I8.to_u8(), *self as u8])
    }

    fn decode(what: &[u8]) -> Result<(i8, usize), String> {
        decode_fixed!(what, Marker::I8, 2, "i8", Ok((what[1] as i8, 2)))
    }
}

impl Encode for i16 {
    fn encode(&self) -> Result<Vec<u8>, String> {
        let mut buf = vec![Marker::I16.to_u8()];
        buf.extend_from_slice(&self.to_le_bytes());
        Ok(buf)
    }

    fn decode(what: &[u8]) -> Result<(i16, usize), String> {
        decode_fixed!(what, Marker::I16, 3, "i16", 
            Ok((i16::from_le_bytes(unsafe{*(what[1..3].as_ptr() as *const [u8; 2])}), 3))
        )
    }
}

impl Encode for i32 {
    fn encode(&self) -> Result<Vec<u8>, String> {
        let mut buf = vec![Marker::I32.to_u8()];
        buf.extend_from_slice(&self.to_le_bytes());
        Ok(buf)
    }

    fn decode(what: &[u8]) -> Result<(i32, usize), String> {
        decode_fixed!(what, Marker::I32, 5, "i32", 
            Ok((i32::from_le_bytes(unsafe{*(what[1..5].as_ptr() as *const [u8; 4])}), 5))
        )
    }
}

impl Encode for i64 {
    fn encode(&self) -> Result<Vec<u8>, String> {
        let mut buf = vec![Marker::I64.to_u8()];
        buf.extend_from_slice(&self.to_le_bytes());
        Ok(buf)
    }

    fn decode(what: &[u8]) -> Result<(i64, usize), String> {
        decode_fixed!(what, Marker::I64, 9, "i64", 
            Ok((i64::from_le_bytes(unsafe{*(what[1..9].as_ptr() as *const [u8; 8])}), 9))
        )
    }
}

impl Encode for f32 {
    fn encode(&self) -> Result<Vec<u8>, String> {
        let mut buf = vec![Marker::F32.to_u8()];
        buf.extend_from_slice(&self.to_le_bytes());
        Ok(buf)
    }

    fn decode(what: &[u8]) -> Result<(f32, usize), String> {
        decode_fixed!(what, Marker::F32, 5, "f32", 
            Ok((f32::from_le_bytes(unsafe{*(what[1..5].as_ptr() as *const [u8; 4])}), 5))
        )
    }
}

impl Encode for f64 {
    fn encode(&self) -> Result<Vec<u8>, String> {
        let mut buf = vec![Marker::F64.to_u8()];
        buf.extend_from_slice(&self.to_le_bytes());
        Ok(buf)
    }

    fn decode(what: &[u8]) -> Result<(f64, usize), String> {
        decode_fixed!(what, Marker::F64, 9, "f64", 
            Ok((f64::from_le_bytes(unsafe{*(what[1..9].as_ptr() as *const [u8; 8])}), 9))
        )
    }
}

impl Encode for Vec<u8> {
    fn encode(&self) -> Result<Vec<u8>, String> {
        let mut buf = vec![Marker::Bin.to_u8()];
        buf.extend_from_slice(&(self.len() as u64).to_le_bytes());
        buf.extend_from_slice(&self);

        Ok(buf)
    }

    fn decode(what: &[u8]) -> Result<(Vec<u8>, usize), String> {
        let what_len = what.len();
        match what_len {
            0 => Err(String::from("can not decode bin: buffer is empty!")),
            _ =>
            match Marker::from_u8(what[0])? {
                Marker::Bin => {
                    if what_len < 9 {
                        return Err(String::from("can not decode bin: invalid buffer size!"))
                    }

                    // decode len
                    let bin_len = u64::from_le_bytes(unsafe{*(what[1..9].as_ptr() as *const [u8; 8])}) as usize;
                    let pos = bin_len + 9;

                    if what_len < pos {
                        return Err(String::from("can not decode bin: invalid buffer size!"))
                    }

                    Ok((what[9..pos].to_vec(), bin_len + 9))
                },
                _ => Err(String::from("can not decode bin: invalid signature!"))
            }
        }
    }
}

impl Encode for String {
    fn encode(&self) -> Result<Vec<u8>, String> {
        let mut buf = vec![Marker::Str.to_u8()];
        buf.extend_from_slice(&(self.len() as u64).to_le_bytes());
        buf.extend_from_slice(self.as_bytes());

        Ok(buf)
    }

    fn decode(what: &[u8]) -> Result<(String, usize), String> {
        let what_len = what.len();
        match what_len {
            0 => Err(String::from("can not decode str: buffer is empty!")),
            _ =>
            match Marker::from_u8(what[0])? {
                Marker::Str => {
                    if what_len < 9 {
                        return Err(String::from("can not decode str: invalid buffer size!"))
                    }

                    // decode len
                    let str_len = u64::from_le_bytes(unsafe{*(what[1..9].as_ptr() as *const [u8; 8])}) as usize;
                    let pos = str_len + 9;

                    if what_len < pos {
                        return Err(String::from("can not decode str: invalid buffer size!"))
                    }

                    // decode str
                    let res = String::from_utf8(what[9..pos].to_vec());

                    match res {
                        Ok(s) => Ok((s, str_len + 9)),
                        Err(_) => Err(String::from("can not decode str: invalid str!"))
                    }
                },
                _ => Err(String::from("can not decode str: invalid signature!"))
            }
        }
    }
}

impl<T: Encode, U: Encode> Encode for (T, U) {
    fn encode(&self) -> Result<Vec<u8>, String> {
        let mut buf = vec![Marker::Pair.to_u8()];
        buf.extend_from_slice(&self.0.encode()?);
        buf.extend_from_slice(&self.1.encode()?);
        Ok(buf)
    }

    fn decode(what: &[u8]) -> Result<((T, U), usize), String> {
        let what_len = what.len();
        match what_len {
            0 => Err(String::from("can not decode pair: buffer is empty!")),
            _ =>
            match Marker::from_u8(what[0])? {
                Marker::Pair => {
                    let mut pos = 1;

                    // decode first
                    let (first, len) = T::decode(&what[pos..])?;
                    pos += len;

                    if what_len < pos {
                        return Err(String::from("can not decode pair: invalid buffer size!"))
                    }

                    // decode second
                    let (second, len) = U::decode(&what[pos..])?;
                    pos += len;

                    Ok(((first, second), pos))
                },
                _ => Err(String::from("can not decode pair: invalid signature!"))
            }
        }
    }
}
