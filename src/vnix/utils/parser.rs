use std::str::Chars;

//-------------------------------------
//-           DEFINITIONS             -
//-------------------------------------

#[derive(Debug, PartialEq, Clone)]
pub enum TokenTree {
    Str(String),
    List(Vec<Token>)
}

#[derive(Debug, PartialEq, Clone)]
pub struct Token {
    pub ident: String,
    pub tree: Option<TokenTree>
}

pub struct Empty;
pub struct Letter;
pub struct Digit;
pub struct Word;
pub struct Int;

pub struct Name {
    pub name: String
}

pub struct Ident<T: Parser> {
    pub ident: String,
    pub it: T
}

pub struct Optional<T: Parser> {
    pub it: T
}

pub struct Concat {
    pub vars: Vec<Box<dyn Parser>>
}

pub struct Alter {
    pub vars: Vec<Box<dyn Parser>>
}

pub struct Exclude<T: Parser> {
    pub it: T
}

pub struct Group<T: Parser> {
    pub it: T
}

pub struct Repeat<T: Parser> {
    pub it: T
}

pub struct ListExtend<T: Parser> {
    pub it: T
}

pub struct ListRmEmpty<T: Parser> {
    pub it: T
}

pub struct ListExpand<T: Parser> {
    pub it: T
}


//-------------------------------------
//-         IMPLEMENTATIONS           -
//-------------------------------------

impl Token {
    pub fn from(ident: &str, s: &str) -> Token {
        Token {
            ident: ident.to_string(),
            tree: Some(TokenTree::Str(s.to_string()))
        }
    }

    pub fn tree(ident: &str, tree: Vec<Token>) -> Token {
        Token {
            ident: ident.to_string(),
            tree: Some(TokenTree::List(tree))
        }
    }

    pub fn empty(ident: &str) -> Token {
        Token {
            ident: ident.to_string(),
            tree: None
        }
    }
}

pub trait Parser{
    fn iter_next(&mut self, it: &mut Chars) -> Option<Token>;
    fn err(&self) -> String;

    fn next(&mut self, it: &mut Chars) -> Result<Token, String> {
        let tmp = (*it).clone();
    
        match self.iter_next(it) {
            Some(v) => Ok(v),
            None => {
                *it = tmp;
                Err(self.err())
            }
        }
    }
}

//empty
impl Parser for Empty {
    fn iter_next(&mut self, _it: &mut Chars) -> Option<Token> {
        Some(Token::empty("EMPTY"))
    }

    fn err(&self) -> String {
        unreachable!()
    }
}

// name
impl Parser for Name {
    fn iter_next(&mut self, it: &mut Chars) -> Option<Token> {
        match it.take(self.name.len()).eq(self.name.chars()) {
            true => Some(Token::from("STRING", &self.name)),
            false => None
        }
    }

    fn err(&self) -> String {
        format!("can not parse name `{}`!", self.name)
    }
}

// letter
impl Parser for Letter {
    fn iter_next(&mut self, it: &mut Chars) -> Option<Token> {
        Some(
            Token::from("STRING", &it.take(1).take_while(|ch| ch.is_alphabetic()).next()?.to_string())
        )
    }

    fn err(&self) -> String {
        "can not parse letter!".into()
    }
}

// digit
impl Parser for Digit {
    fn iter_next(&mut self, it: &mut Chars) -> Option<Token> {
        Some(
            Token::from("INT", &it.take(1).take_while(|ch| ch.is_digit(10)).next()?.to_string())
        )
    }

    fn err(&self) -> String {
        "can not parse digit!".into()
    }
}

// word
impl Parser for Word {
    fn iter_next(&mut self, it: &mut Chars) -> Option<Token> {
        let mut res = String::new();
        let mut p = Letter;

        loop {
            match p.next(it) {
                Ok(v) =>
                match v.tree {
                    Some(t) =>
                    match t {
                        TokenTree::Str(s) => res += &s,
                        _ => ()
                    },
                    None => ()
                },
                Err(_) => return
                match res.as_str(){
                    "" => None,
                    _ => Some(Token::from("STRING", &res))
                }
            }
        }
    }

    fn err(&self) -> String {
        "can not parse word!".into()
    }
}

// int
impl Parser for Int {
    fn iter_next(&mut self, it: &mut Chars) -> Option<Token> {
        let mut res = String::new();
        let mut p = Digit;

        loop {
            match p.next(it) {
                Ok(v) =>
                match v.tree {
                    Some(t) =>
                    match t {
                        TokenTree::Str(s) => res += &s,
                        _ => ()
                    },
                    None => ()
                },
                Err(_) => return
                match res.as_str(){
                    "" => None,
                    _ => Some(Token::from("INT", &res))
                }
            }
        }
    }

    fn err(&self) -> String {
        "can not parse int!".into()
    }
}

// ident
impl<T: Parser> Parser for Ident<T> {
    fn iter_next(&mut self, it: &mut Chars) -> Option<Token> {
        match self.it.next(it) {
            Ok(v) => Some(
                Token{
                    ident: self.ident.clone(),
                    tree: v.tree
                }
            ),
            Err(_) => None
        }
    }

    fn err(&self) -> String {
        format!("can not parse ident `{}`: {}", self.ident, self.it.err())
    }
}

// option
impl<T: Parser> Parser for Optional<T> {
    fn iter_next(&mut self, it: &mut Chars) -> Option<Token> {
        match self.it.next(it) {
            Ok(v) => Some(v),
            Err(_) => Some(Token::empty("EMPTY"))
        }
    }

    fn err(&self) -> String {
        unreachable!()
    }
}

// concat
impl Parser for Concat {
    fn iter_next(&mut self, it: &mut Chars) -> Option<Token> {
        let mut res: Vec<Token> = Vec::new();

        for p in self.vars.iter_mut() {
            match p.next(it) {
                Ok(v) => res.push(v),
                Err(_) => return None
            }
        }

        Some(
            Token{
                ident: "LIST".into(),
                tree: Some(TokenTree::List(res))
            }
        )
    }

    fn err(&self) -> String {
        "can not parse concat!".into()
    }
}

// alter
impl Parser for Alter {
    fn iter_next(&mut self, it: &mut Chars) -> Option<Token> {
         for p in self.vars.iter_mut() {
            match p.next(it) {
                Ok(v) => return Some(v),
                Err(_) => ()
            }
        }

        None
    }

    fn err(&self) -> String {
        "can not parse alter!".into()
    }
}

// exclude
impl<T: Parser> Parser for Exclude<T> {
    fn iter_next(&mut self, it: &mut Chars) -> Option<Token> {
        match self.it.next(it) {
            Ok(_) => Some(Token::empty("EMPTY")),
            Err(_) => None
        }
    }

    fn err(&self) -> String {
        self.it.err()
    }
}

// group
impl<T: Parser> Parser for Group<T> {
    fn iter_next(&mut self, it: &mut Chars) -> Option<Token> {
        match self.it.next(it) {
            Ok(v) => Some(
                Token::tree("GROUP", vec![v])
            ),
            Err(_) => None
        }
    }

    fn err(&self) -> String {
        format!("can not parse group: {}", self.it.err())
    }
}

// space
pub fn space() -> Exclude<Name> {
    Exclude {
        it: Name {
            name: " ".into()
        }
    }
}

// repeat
impl<T: Parser> Parser for Repeat<T> {
    fn iter_next(&mut self, it: &mut Chars) -> Option<Token> {
        let mut res: Vec<Token> = Vec::new();

        loop {
            match self.it.next(it) {
                Ok(v) => res.push(v),
                Err(_) => return
                match res.len() {
                    0 => Some(Token::empty("EMPTY")),
                    _ =>Some(
                        Token{
                            ident: "LIST".into(),
                            tree: Some(TokenTree::List(res))
                        }
                    )
                }
            }
        }
    }

    fn err(&self) -> String {
        self.it.err()
    }
}

// spaces
pub fn spaces() -> Exclude<Repeat<Name>> {
    Exclude {
        it: Repeat{
            it: Name{
                name: " ".into()
            }
        }
    }
}

// list handlers
impl<T: Parser> Parser for ListExtend<T> {
    fn iter_next(&mut self, it: &mut Chars) -> Option<Token> {
        match self.it.next(it) {
            Ok(v) =>
            match v.tree.clone() {
                Some(tree) =>
                match tree {
                    TokenTree::List(list) => {
                        let mut new_list: Vec<Token> = Vec::new();

                        for t in list {
                            match t.tree {
                                Some(ttree) =>
                                match ttree{
                                    TokenTree::List(tlist) => new_list.extend(tlist.iter().cloned()),
                                    _ => ()
                                },
                                None => new_list.push(t)
                            }
                        }

                        Some(Token::tree(&v.ident, new_list))
                    },
                    _ => return Some(v)
                },
                None => return Some(v)
            },
            Err(_) => None
        }
    }

    fn err(&self) -> String {
        self.it.err()
    }
}

impl<T: Parser> Parser for ListRmEmpty<T> {
    fn iter_next(&mut self, it: &mut Chars) -> Option<Token> {
        match self.it.next(it) {
            Ok(v) =>
            match v.tree.clone() {
                Some(tree) =>
                match tree {
                    TokenTree::List(list) => {
                        let mut new_list: Vec<Token> = Vec::new();

                        for t in list {
                            match t.ident.as_str() {
                                "EMPTY" => (),
                                _ => new_list.push(t)
                            }
                        }

                        Some(Token::tree(&v.ident, new_list))
                    },
                    _ => return Some(v)
                },
                None => return Some(v)
            },
            Err(_) => None
        }
    }

    fn err(&self) -> String {
        self.it.err()
    }
}

impl<T: Parser> Parser for ListExpand<T> {
    fn iter_next(&mut self, it: &mut Chars) -> Option<Token> {
        match self.it.next(it) {
            Ok(v) =>
            match v.tree.clone() {
                Some(tree) =>
                match tree {
                    TokenTree::List(list) => {
                        match list.len() {
                            1 => return Some(list[0].clone()),
                            _ => return Some(v)
                        } 
                    },
                    _ => return Some(v)
                },
                None => return Some(v)
            },
            Err(_) => None
        }
    }

    fn err(&self) -> String {
        self.it.err()
    }
}