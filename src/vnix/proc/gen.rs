use super::super::core::slot::Slot;
use super::super::core::node::Proc;
use super::super::core::msg::Msg;
use super::super::core::types::{Int, Float};
use super::super::msg;

//-------------------------------------
//-           DEFINITIONS             -
//-------------------------------------

pub struct GenInt;
pub struct GenFloat;


//-------------------------------------
//-         IMPLEMENTATIONS           -
//-------------------------------------

// gen.int
impl GenInt {
    fn val(val: i64) -> Msg {
        msg::new_int(Int::Def(val))
    }
}

impl Proc for GenInt {
    fn process(slot: &mut Slot<Self>) -> Result<(), String> {
        let msg = GenInt::val(123);
        slot.out_port(0).send(msg)?;
        
        Ok(())
    }
}

// gen.float
impl GenFloat {
    fn val(val: f64) -> Msg {
        msg::new_float(Float::Def(val))
    }
}

impl Proc for GenFloat {
    fn process(slot: &mut Slot<Self>) -> Result<(), String> {
        let msg = GenFloat::val(3.14);
        slot.out_port(0).send(msg)?;
        
        Ok(())
    }
}

// TODO: gen.str
// TODO: gen.ui
