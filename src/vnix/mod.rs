use std::{thread, time};
use std::io::{self, Write};
use std::fs::File;
use nix::sys;
use nix::mount;

pub mod core;
pub mod utils;
pub mod msg;
pub mod proc;

// TODO: refactoring: simplify host error handling, make code more clear

macro_rules! host_panic {
    ($what:expr, $msg:expr) => {
        match $what {
            Err(_) => {
                println!($msg);
                println!("INFO [host]: looks like the vnix system is down, please, reboot your device manually.");
                loop { }
            },
            Ok(v) => v
        }
    };
}

macro_rules! host_err {
    ($what:expr, $msg:expr) => {
        match $what {
            Err(_) => {
                println!($msg);
            },
            Ok(_) => ()
        }
    };
}

macro_rules! mount_def {
    ($tgt:expr, $fs:expr, $err:expr) => {
        host_panic!(mount::mount::<str, str, str, str>(None, $tgt, Some($fs), mount::MsFlags::empty(), None), $err);
    };
}

pub fn init() {
    /*
        clear
        mount -t devtmpfs none /dev
        mount -t proc none /proc
        mount -t sysfs none /sys
    */

    print!("\x1bc");
    host_err!(io::stdout().flush(), "ERROR [host]: can not flush term!");

    mount_def!("/dev", "devtmpfs", "CRIT [host]: unable to mount devtmpfs!");
    mount_def!("/proc", "proc", "CRIT [host]: unable to mount proc!");
    mount_def!("/sys", "sysfs", "CRIT [host]: unable to mount sysfs!");
    host_err!(sys::reboot::set_cad_enabled(false), "ERROR [host]: unable to disable Ctrl-Alt-Del reboot!");

    // echo 2 > /proc/sys/kernel/printk
    host_err!(
        host_panic!(
            File::create("/proc/sys/kernel/printk"),
            "CRIT [host]: unable to open linux 'klog' file!")
        .write_all(b"2"),
        "ERROR [host]: unable to set linux log level!");
}

pub fn poweroff(secs: u64) {
    print!("INFO [host]: poweroff ");
    for sec in (1..secs + 1).rev() {
        print!("{} ", sec);
        host_err!(io::stdout().flush(), "ERROR [host]: can not flush term!");
        thread::sleep(time::Duration::from_secs(1));
    }
    
    host_panic!(
        sys::reboot::reboot(sys::reboot::RebootMode::RB_POWER_OFF),
        "CRIT [host]: unable to shutdown system!");
}
