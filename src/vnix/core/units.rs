use num::bigint::{Sign, BigInt};
use num::rational::BigRational;
use super::super::utils::pack::{Encode, Marker};
// use num::complex::Complex;

//-------------------------------------
//-           DEFINITIONS             -
//-------------------------------------

#[derive(Debug, PartialEq, PartialOrd)]
pub enum Int {
    Def(i64),
    Big(BigInt)
}

#[derive(Debug, PartialEq, PartialOrd)]
pub enum Float {
    Def(f64),
    Big(BigRational)
}

#[derive(Debug, PartialEq)]
pub enum Unit {
    Bool(bool),
    Byte(u8),
    Int(Int),
    Float(Float),
    // Complex(Complex<Float>),
    Str(String),
    List(Vec<Unit>),
    Map(Vec<(Unit, Unit)>),
    Msg(Vec<(String, Unit)>)
}

#[derive(Debug, Clone, Copy)]
pub enum UnitMarker {
    BigInt = 15,
    BigRational,
    List,
    Map,
    Msg
}


//-------------------------------------
//-         IMPLEMENTATIONS           -
//-------------------------------------

impl UnitMarker {
    pub fn to_u8(&self) -> u8 {
        *self as u8
    }

    pub fn from_u8(what: u8) -> Result<UnitMarker, String> {
        match what {
            sig if sig == UnitMarker::BigInt.to_u8() => Ok(UnitMarker::BigInt),
            sig if sig == UnitMarker::BigRational.to_u8() => Ok(UnitMarker::BigRational),
            sig if sig == UnitMarker::List.to_u8() => Ok(UnitMarker::List),
            sig if sig == UnitMarker::Map.to_u8() => Ok(UnitMarker::Map),
            sig if sig == UnitMarker::Msg.to_u8() => Ok(UnitMarker::Msg),
            _ => Err(String::from("can not decode marker: unknow signature!"))
        }
    }
}

impl Encode for Sign {
    fn encode(&self) -> Result<Vec<u8>, String> {
        match self {
            Sign::Minus => Ok(vec![0x1]),
            Sign::Plus => Ok(vec![0]),
            _ => Err(String::from("can not encode sign: invalid value!"))
        }
    }

    fn decode(what: &[u8]) -> Result<(Sign, usize), String> {
        match what.len() {
            0 => Err(String::from("can not decode sign: buffer is empty!")),
            l if l >= 1 => 
            match what[0] {
                0x1 => Ok((Sign::Minus, 1)),
                0 => Ok((Sign::Plus, 1)),
                _ => Err(String::from("can not decode sign: invalid signature!"))
            },
            _ => Err(String::from("can not decode sign: invalid buffer size!"))
        }
    }
}

impl Encode for Unit {
    fn encode(&self) -> Result<Vec<u8>, String> {
        match self {
            Unit::Bool(unit) => unit.encode(),
            Unit::Byte(unit) => unit.encode(),
            Unit::Int(unit) =>
            match unit {
                Int::Def(val) => val.encode(),
                Int::Big(val) => {
                    let mut buf: Vec<u8> = vec![UnitMarker::BigInt.to_u8()];
                    buf.extend_from_slice(&val.to_bytes_le().encode()?);
                    Ok(buf)
                }
            },
            Unit::Float(unit) =>
            match unit {
                Float::Def(val) => val.encode(),
                Float::Big(val) => {
                    let mut buf: Vec<u8> = vec![UnitMarker::BigRational.to_u8()];
                    buf.extend_from_slice(&(val.numer().to_bytes_le(), val.denom().to_bytes_le()).encode()?);
                    Ok(buf)
                }
            },
            Unit::Str(unit) => unit.encode(),
            Unit::List(unit) => {
                let mut buf: Vec<u8> = vec![UnitMarker::List.to_u8()];
                let mut subuf: Vec<u8> = Vec::new();

                for u in unit {
                    subuf.extend_from_slice(&u.encode()?);
                }

                buf.extend_from_slice(&(unit.len() as i64, subuf).encode()?);
                Ok(buf)
            },
            Unit::Map(unit) => {
                let mut buf: Vec<u8> = vec![UnitMarker::Map.to_u8()];
                let mut subuf: Vec<u8> = Vec::new();

                for u in unit {
                    subuf.extend_from_slice(&u.encode()?);
                }

                buf.extend_from_slice(&(unit.len() as i64, subuf).encode()?);
                Ok(buf)
            },
            Unit::Msg(unit) => {
                let mut buf: Vec<u8> = vec![UnitMarker::Msg.to_u8()];
                let mut subuf: Vec<u8> = Vec::new();

                for u in unit {
                    subuf.extend_from_slice(&u.encode()?);
                }

                buf.extend_from_slice(&(unit.len() as i64, subuf).encode()?);
                Ok(buf)
            }
        }
    }

    fn decode(what: &[u8]) -> Result<(Unit, usize), String> {
        match Marker::from_u8(what[0]) {
            Ok(sig) =>
            match sig {
                Marker::Bool => {
                    let decoder = bool::decode(what)?;
                    Ok((Unit::Bool(decoder.0), decoder.1))
                },
                Marker::U8 => {
                    let decoder = u8::decode(what)?;
                    Ok((Unit::Byte(decoder.0), decoder.1))
                },
                Marker::I64 => {
                    let decoder = i64::decode(what)?;
                    Ok((Unit::Int(Int::Def(decoder.0)), decoder.1))
                },
                Marker::F64 => {
                    let decoder = f64::decode(what)?;
                    Ok((Unit::Float(Float::Def(decoder.0)), decoder.1))
                },
                Marker::Str => {
                    let decoder = String::decode(what)?;
                    Ok((Unit::Str(decoder.0), decoder.1))
                },
                _ => Err(String::from("can not decode unit: unknow signature!"))
            },
            Err(_) =>
            match UnitMarker::from_u8(what[0])? {
                UnitMarker::BigInt => {
                    let (big, len) = <(Sign, Vec<u8>)>::decode(&what[1..])?;
                    Ok((Unit::Int(Int::Big(BigInt::from_bytes_le(big.0, &big.1))), len))
                },
                UnitMarker::BigRational => {
                    let ((numer, denom), len) = <((Sign, Vec<u8>), (Sign, Vec<u8>))>::decode(&what[1..])?;
                    Ok((Unit::Float(Float::Big(BigRational::new(BigInt::from_bytes_le(numer.0, &numer.1), BigInt::from_bytes_le(denom.0, &denom.1)))), len))
                },
                UnitMarker::List => {
                    let ((list_len, buf), len) = <(i64, Vec<u8>)>::decode(&what[1..])?;
                    let mut res: Vec<Unit> = Vec::new();

                    let mut pos = 0;
                    for _ in 0..list_len {
                        let (unit, dec_len) = Unit::decode(&buf[pos..])?; 
                        res.push(unit);
                        pos += dec_len;
                    }

                    Ok((Unit::List(res), len))
                },
                UnitMarker::Map => {
                    let ((map_len, buf), len) = <(i64, Vec<u8>)>::decode(&what[1..])?;
                    let mut res: Vec<(Unit, Unit)> = Vec::new();

                    let mut pos = 0;
                    for _ in 0..map_len {
                        let (pair, dec_len) = <(Unit, Unit)>::decode(&buf[pos..])?; 
                        res.push(pair);
                        pos += dec_len;
                    }

                    Ok((Unit::Map(res), len))
                },
                UnitMarker::Msg => {
                    let ((msg_len, buf), len) = <(i64, Vec<u8>)>::decode(&what[1..])?;
                    let mut res: Vec<(String, Unit)> = Vec::new();

                    let mut pos = 0;
                    for _ in 0..msg_len {
                        let (pair, dec_len) = <(String, Unit)>::decode(&buf[pos..])?; 
                        res.push(pair);
                        pos += dec_len;
                    }

                    Ok((Unit::Msg(res), len))
                }
            }
        }
    }
}
