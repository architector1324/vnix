use crossbeam::channel::{Sender, Receiver, bounded};
use std::time::Duration;
use super::msg::Msg;


#[derive(Debug)]
pub struct Port{
    pub id: u64,
    chan: (Sender<Msg>, Receiver<Msg>)
}

impl Port {
    pub fn new(id: u64) -> Port {
        Port {
            id,
            chan: bounded(1)
        }
    }

    // send message to port
    pub fn send(&mut self, msg: Msg) -> Result<(), String> {
        match self.chan.0.send_timeout(msg.clone(), Duration::from_secs(2)) {
            Ok(none) => Ok(none),
            Err(_) => Err(format!("port{}: can not send msg {}: timed out!", self.id, msg))
        }
    }

    // receive message from port
    pub fn recv(&mut self) -> Result<Msg, String> {
        match self.chan.1.recv_timeout(Duration::from_secs(2)) {
            Ok(msg) => Ok(msg),
            Err(_) => Err(format!("port{}: can not recv msg: timed out!", self.id))
        }
    }
}