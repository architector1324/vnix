use std::collections::HashMap;
use std::collections::hash_map::Entry;
use std::marker::PhantomData;
use super::slot::Slot;


pub trait Proc: Sized {
    fn process(slot: &mut Slot<Self>) -> Result<(), String>;
}

pub struct Node<P: Proc> {
    pub name: &'static str,
    slots: HashMap<u64, Slot<P>>,
    _phantom: PhantomData<P>
}

impl<P: Proc> Node<P> {
    pub fn new(name:&'static str) -> Node<P> {
        Node::<P> {
            name,
            slots: HashMap::new(),
            _phantom: PhantomData
        }
    }

    pub fn slot(&mut self, id: u64) -> &mut Slot<P> {
        match self.slots.entry(id) {
            Entry::Occupied(slot) => slot.into_mut(),
            Entry::Vacant(v) => v.insert(Slot::<P>::new(id))
        }
    }

    pub fn clear(&mut self) {
        self.slots.clear();
    }
}