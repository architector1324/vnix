use std::collections::HashMap;
use std::collections::hash_map::Entry;
use std::marker::PhantomData;
use super::port::Port;
use super::node::Proc;


pub struct Slot<P: Proc> {
    pub id: u64,
    in_ports: HashMap<u64, Port>,
    out_ports: HashMap<u64, Port>,
    _phantom: PhantomData<P>
}

impl<P: Proc> Slot<P> {
    pub fn new(id: u64) -> Slot<P> {
        Slot::<P> {
            id,
            in_ports: HashMap::new(),
            out_ports: HashMap::new(),
            _phantom: PhantomData
        }
    }

    pub fn in_port(&mut self, id: u64) -> &mut Port {
        match self.in_ports.entry(id) {
            Entry::Occupied(port) => port.into_mut(),
            Entry::Vacant(v) => v.insert(Port::new(id))
        }
    }

    pub fn out_port(&mut self, id: u64) -> &mut Port {
        match self.out_ports.entry(id) {
            Entry::Occupied(port) => port.into_mut(),
            Entry::Vacant(v) => v.insert(Port::new(id))
        }
    }

    pub fn gen(&mut self) -> Result<(), String> {
        P::process(self)
    }

    pub fn clear(&mut self) {
        self.in_ports.clear();
        self.out_ports.clear();
    }
}