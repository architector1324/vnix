use super::types;
use std::sync::Arc;
use std::fmt;
use std::slice::Iter;


#[derive(Debug, PartialEq, Clone)]
pub struct Msg(Arc<Box<Vec<(types::Type, types::Type)>>>);

impl fmt::Display for Msg {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = self.iter().fold(String::new(), |acc, pair| format!("{}{}:{} ", acc, &pair.0, &pair.1));
        write!(f, "{{{}}}", s.trim_end())
    }
}

impl Msg {
    pub fn new() -> Msg {
        Msg(Arc::new(Box::new(vec![])))
    }

    pub fn from(map: Vec<(types::Type, types::Type)>) -> Msg {
        Msg(Arc::new(Box::new(map)))
    }

    pub fn iter(&self) -> Iter<(types::Type, types::Type)> {
        (&**self.0).iter()
    }
}