use num::bigint::BigInt;
use num::rational::BigRational;
use std::fmt;

//-------------------------------------
//-           DEFINITIONS             -
//-------------------------------------

#[derive(Debug, PartialEq, PartialOrd)]
pub enum Int {
    Def(i64),
    Big(BigInt)
}

#[derive(Debug, PartialEq, PartialOrd)]
pub enum Float {
    Def(f64),
    Big(BigRational)
}

#[derive(Debug, PartialEq, PartialOrd)]
pub enum Type {
    Bool(bool),
    Byte(u8),
    Int(Int),
    Float(Float),
    Str(String)
}


//-------------------------------------
//-         IMPLEMENTATIONS           -
//-------------------------------------

impl fmt::Display for Type {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Type::Bool(t) =>
            match t{
                true => write!(f, "t"),
                false => write!(f, "f")
            },
            Type::Byte(t) => write!(f, "{:x}", t),
            Type::Int(t) =>
            match t {
                Int::Def(val) => write!(f, "{}", val),
                Int::Big(val) => unimplemented!()
            },
            Type::Float(t) =>
            match t {
                Float::Def(val) => write!(f, "{}", val),
                Float::Big(val) => unimplemented!()
            },
            Type::Str(t) => write!(f, "`{}`", t),
            _ => unimplemented!()
        }
    }
}