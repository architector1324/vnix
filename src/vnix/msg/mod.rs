use super::core::types::{Type, Int, Float};
use super::core::msg::Msg;

pub fn new_bool(val: bool) -> Msg{
    Msg::from(vec![
        (Type::Str("type".into()), Type::Str("bool".into())),
        (Type::Str("val".into()), Type::Bool(val))
    ])
}

pub fn new_byte(val: u8) -> Msg{
    Msg::from(vec![
        (Type::Str("type".into()), Type::Str("byte".into())),
        (Type::Str("val".into()), Type::Byte(val))
    ])
}

pub fn new_int(val: Int) -> Msg{
    Msg::from(vec![
        (Type::Str("type".into()), Type::Str("int".into())),
        (Type::Str("val".into()), Type::Int(val))
    ])
}

pub fn new_float(val: Float) -> Msg{
    Msg::from(vec![
        (Type::Str("type".into()), Type::Str("float".into())),
        (Type::Str("val".into()), Type::Float(val))
    ])
}

pub fn new_str(dat: String) -> Msg{
    Msg::from(vec![
        (Type::Str("type".into()), Type::Str("str".into())),
        (Type::Str("dat".into()), Type::Str(dat))
    ])
}