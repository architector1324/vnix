![](./doc/vnix_logo.png)

This operating system is a proof of concept, i made it just for fun. Now it has a very draft kernel written in [**Rust**](https://www.rust-lang.org/).

**Virtual Networking Information and Computing System (Vnix)** – it is an operating system with non-Turing virtual networking architecture following principles:

- Technical:
  - **Operating System** is an set of *applications*, that provide user to control the computer. 
  - **Application** is an abstraction over a network of *nodes* that communicating by *messages* (some variant of [SOA](https://en.wikipedia.org/wiki/Service-oriented_architecture)).
  - **Node** is an interface to OS functionality that solves only one task and does it well. Unlike [microservice](https://en.wikipedia.org/wiki/Microservices), node solves a group of a very similar subtasks that are a variant of the same task. For example, output text, graphics, user interface, etc. to terminal.
  - **Message** is an information package and nodes communication unit, that consists of *data units* (very similar to [json](https://www.json.org), but more powerful).
  - **Data Unit** (or just unit) is an minimal information unit, that represents some data, like numbers, strings, lists, lazy data generators and etc.

- Conceptual:
  - Make something once and reuse it! ([DRY](https://ru.wikipedia.org/wiki/Don%E2%80%99t_repeat_yourself))
  - If you can make it simple, keep it simple ([KISS](https://en.wikipedia.org/wiki/KISS_principle)).
  - If you make something it should work everywhere ([cross-platform](https://en.wikipedia.org/wiki/Cross-platform_software)).


## Goals
- Let user make the computer to do what he want in the easiest way. 
- Provide a very simple and in the same time powerful operating system.
- Once made software should work on any device, regardless of its architecture.

## Design
- **Vnix kernel** is virtual and works like one big init process on the **unix** host kernel. It emulates nodes network and use host kernel for communication with hardware.
- User space is platform-independent and all software should work on any device.
- By default **linux** kernel is using as host. But also it’s a good practise to use some unikernel as host.
- In plans it would be a great idea to write own tiny host kernel specially for vnix (based on [rust-x86](https://github.com/gz/rust-x86)).

![](./doc/design.png)



## FAQ
Comming soon ...